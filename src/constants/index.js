const X = 'X'
const O = 'O'
const SIDESIZE = 3
const BOARDSIZE = SIDESIZE * SIDESIZE
const SCOREFILE = 'score.csv'

module.exports = {
  X,
  O,
  BOARDSIZE,
  SCOREFILE,
}
